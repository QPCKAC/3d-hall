% Load your figure into MATLAB
fig =openfig('Crash-3-30.fig');
% Get handles to the subplots
subplot_handles = get(fig, 'Children');

% Initialize x and y as cell arrays
x = cell(length(subplot_handles), 1);
y = cell(length(subplot_handles), 1);

% Loop through each subplot and extract the data
for i = 1:length(subplot_handles)
    
    % Get handles to the line objects within this subplot
    line_handles = get(subplot_handles(i), 'Children');
    
    % Extract the x and y data for the first line object (assuming there is only one)
    xdata = get(line_handles(1), 'XData');
    ydata = get(line_handles(1), 'YData');
    
    % Store the data in separate arrays for each subplot
    x{i} = xdata;
    y{i} = ydata;
end

% Define filename for CSV output
filename = '3-30-data_p1.csv';

% Open the file for writing
fid = fopen(filename, 'w');

% Write the x and y data to the file
for i = 1:length(subplot_handles)
    
    % Write a header line indicating the subplot number
    fprintf(fid, 'Subplot %d\n', i);
    
    % Write the x and y data to the file
    fprintf(fid, 'x,y\n');
    for j = 1:length(x{i})
        fprintf(fid, '%f,%f\n', x{i}(j), y{i}(j));
    end
    
    % Write a blank line to separate subplots
    fprintf(fid, '\n');
end

% Close the file
fclose(fid);

% Print a message indicating that the data has been exported
fprintf('Data has been exported to %s\n', filename);

