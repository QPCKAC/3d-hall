clear all;
close all;
clc;

h=openfig('Crashed_Test2_b_ON.fig');
h = findobj (gca, 'Type', 'line')
x = get(h, 'Xdata');
y = get(h, 'Ydata');

A=[];
A(:, 1)=x
A(:, 2)=y
csvwrite('Crashed_Test2_b_ON.csv', A)